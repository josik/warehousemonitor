using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WarehouseMonitor.Console.Domain;

namespace WarehouseMonitor.Console.Infrastructure.Out
{
    public class TextWarehouseFormatterStrategy : IWarehouseFormatterStrategy
    {
        private readonly IComparer<KeyValuePair<Material, int>> _materialComparer;

        public TextWarehouseFormatterStrategy(IComparer<KeyValuePair<Material, int>> materialComparer)
        {
            this._materialComparer = materialComparer;
        }

        public string Format(Warehouse warehouse)
        {
            if (warehouse == null) throw new ArgumentNullException(nameof(warehouse), "Warehouse could not be null");

            StringBuilder builder = new StringBuilder();
            builder.AppendLine($"{warehouse.Name} (total {warehouse.MaterialsCount})");

            var materials = warehouse.Materials.ToList();
            materials.Sort(_materialComparer);

            foreach (var material in materials)
            {
                builder.AppendLine($"{material.Key.Id}: {material.Value}");
            }
            return builder.ToString();
        }
    }
}