using System.Collections.Generic;
using WarehouseMonitor.Console.Domain;

namespace WarehouseMonitor.Console.Infrastructure.Out
{
    public interface IWarehouseFormatter
    {
        string Format(IEnumerable<Warehouse> warehouses);
    }
}