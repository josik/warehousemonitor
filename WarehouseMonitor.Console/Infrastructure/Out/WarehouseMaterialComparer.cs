using System;
using System.Collections.Generic;
using WarehouseMonitor.Console.Domain;

namespace WarehouseMonitor.Console.Infrastructure.Out
{
    public class WarehouseMaterialComparer : IComparer<KeyValuePair<Material, int>>
    {
        public int Compare(KeyValuePair<Material, int> x, KeyValuePair<Material, int> y) => x.Key.Id.CompareTo(y.Key.Id);
    }
}