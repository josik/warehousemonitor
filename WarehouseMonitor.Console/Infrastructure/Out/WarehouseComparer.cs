using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using WarehouseMonitor.Console.Domain;

namespace WarehouseMonitor.Console.Infrastructure.Out
{
    public class WarehouseComparer : IComparer<Warehouse>
    {
        public int Compare([AllowNull] Warehouse x, [AllowNull] Warehouse y)
        {
            int amountComparisonResult = y.MaterialsCount.CompareTo(x.MaterialsCount);

            if (amountComparisonResult == 0)
            {
                return y.Name.CompareTo(x.Name);
            }

            return amountComparisonResult;
        }
    }
}