namespace WarehouseMonitor.Console.Infrastructure.Out
{
    public class TextWarehousesFormatterFactory
    {
        public static IWarehouseFormatter Create()
        {
            var warehouseMaterialComparer = new WarehouseMaterialComparer();
            var warehouseComparer = new WarehouseComparer();
            var formatterStrategy = new TextWarehouseFormatterStrategy(warehouseMaterialComparer);
            return new WarehouseFormatter(formatterStrategy, warehouseComparer);
        }
    }
}