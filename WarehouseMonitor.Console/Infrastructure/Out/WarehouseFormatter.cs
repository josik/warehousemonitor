using System.Collections.Generic;
using System.Text;
using WarehouseMonitor.Console.Domain;
using System.Linq;

namespace WarehouseMonitor.Console.Infrastructure.Out
{
    public class WarehouseFormatter : IWarehouseFormatter
    {
        private readonly IWarehouseFormatterStrategy _formatterStrategy;
        private readonly IComparer<Warehouse> _comparer;

        public WarehouseFormatter(IWarehouseFormatterStrategy formatterStrategy, IComparer<Warehouse> comparer)
        {
            _formatterStrategy = formatterStrategy;
            _comparer = comparer;
        }

        public string Format(IEnumerable<Warehouse> inputWarehouses)
        {
            StringBuilder builder = new StringBuilder();
            var warehouses = inputWarehouses.ToList();
            warehouses.Sort(_comparer);

            foreach (var warehouse in warehouses)
            {
                builder.AppendLine(_formatterStrategy.Format(warehouse));
            }

            return builder.ToString();
        }
    }
}