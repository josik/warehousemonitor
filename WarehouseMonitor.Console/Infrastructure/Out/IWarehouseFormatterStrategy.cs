using WarehouseMonitor.Console.Domain;

namespace WarehouseMonitor.Console.Infrastructure.Out
{
    public interface IWarehouseFormatterStrategy
    {
        string Format(Warehouse warehouse);
    }
}