using System.Collections.Generic;
using WarehouseMonitor.Console.Domain;

namespace WarehouseMonitor.Console.Infrastructure.In
{
    public class WarehouseParser : IWarehouseParser
    {
        private readonly IWarehouseParseStrategy parseStrategy;

        public WarehouseParser(IWarehouseParseStrategy parseStrategy)
        {
            this.parseStrategy = parseStrategy;
        }
        public IEnumerable<Warehouse> Parse(string inpuText)
        {
            var warehouseList = new Dictionary<string, Warehouse>();

            foreach (var inputLine in  parseStrategy.Split(inpuText))
            {
                var importItem = parseStrategy.Parse(inputLine);
                if (importItem == null) continue;

                foreach (var availability in importItem.Availabilities)
                {
                    if (warehouseList.ContainsKey(availability.WarehouseName))
                    {
                        warehouseList[availability.WarehouseName].AddMaterial(importItem.Material, availability.MaterialAmount);
                        continue;
                    }

                    var warehouse = new Warehouse(availability.WarehouseName);
                    warehouse.AddMaterial(importItem.Material, availability.MaterialAmount);
                    warehouseList.Add(warehouse.Name, warehouse);
                }
            }

            return warehouseList.Values;
        }
    }
}