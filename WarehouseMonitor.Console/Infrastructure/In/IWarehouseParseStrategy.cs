namespace WarehouseMonitor.Console.Infrastructure.In
{
    public interface IWarehouseParseStrategy
    {
        WarehouseImportItem Parse(string inputTextLine);
        string[] Split(string inputText);
    }
}