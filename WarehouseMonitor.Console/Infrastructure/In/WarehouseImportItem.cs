using System.Collections.Generic;
using WarehouseMonitor.Console.Domain;

namespace WarehouseMonitor.Console.Infrastructure.In
{
    public class WarehouseImportItem
    {
        public Material Material { get; private set; }
        public IEnumerable<WarehouseMaterialAvailability> Availabilities { get; private set; }

        public WarehouseImportItem(Material material, IEnumerable<WarehouseMaterialAvailability> availabilities)
        {
            Material = material;
            Availabilities = availabilities;
        }

        public class WarehouseMaterialAvailability
        {
            public string WarehouseName { get; private set; }
            public int MaterialAmount { get; private set; }

            public WarehouseMaterialAvailability(string warehouseName, int materialAmount)
            {
                WarehouseName = warehouseName;
                MaterialAmount = materialAmount;
            }
        }
    }
}