namespace WarehouseMonitor.Console.Infrastructure.In
{
    public class TextWarehouseParserFactory
    {
        public static IWarehouseParser Create()
        {
            var warehouseParser = new TextWarehouseParseStrategy();
            return new WarehouseParser(warehouseParser);
        }
    }
}