using System.Collections.Generic;
using WarehouseMonitor.Console.Domain;
using static WarehouseMonitor.Console.Infrastructure.In.WarehouseImportItem;

namespace WarehouseMonitor.Console.Infrastructure.In
{
    public class TextWarehouseParseStrategy : IWarehouseParseStrategy
    {
        public string[] Split(string inputString){
            return inputString.Split(new [] {'\n', '\r'});
        }

        //EX: Yankee Hardware 110 Deg. Hinge;COM-123908;WH-A,10|WH-B,11
        public WarehouseImportItem Parse(string inputString)
        {
            if (inputString.StartsWith('#')) return null;
            //TODO: Add regex check if string is correct

            string[] stringParts = inputString.Split(';');

            var material = new Material(stringParts[1], stringParts[0]);
            var availabilities = new List<WarehouseMaterialAvailability>();

            foreach (string availabilityString in stringParts[2].Split('|'))
            {
                var availabilityParts = availabilityString.Split(',');
                availabilities.Add(new WarehouseMaterialAvailability(availabilityParts[0], int.Parse(availabilityParts[1])));
            }

            return new WarehouseImportItem(material, availabilities);
        }
    }
}