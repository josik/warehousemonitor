using System.Collections.Generic;
using WarehouseMonitor.Console.Domain;

namespace WarehouseMonitor.Console.Infrastructure.In
{
    public interface IWarehouseParser
    {
        IEnumerable<Warehouse> Parse(string inputText);
    }
}