using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace WarehouseMonitor.Console.Domain
{
    public class Warehouse
    {
        private IDictionary<Material, int> _materials;

        public string Name { get; private set; }
        public int MaterialsCount { get; private set; }
        public IReadOnlyDictionary<Material, int> Materials => new ReadOnlyDictionary<Material, int>(_materials);

        public Warehouse(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name), "Warehouse could not have an empty name");

            Name = name;
            _materials = new Dictionary<Material, int>();
        }

        public void AddMaterial(Material material, int newAmount)
        {
            MaterialsCount += newAmount;

            if (_materials.ContainsKey(material))
            {
                _materials[material] += newAmount;
                return;
            }

            _materials.Add(material, newAmount);
        }

        public override int GetHashCode() => Name.GetHashCode();
        public override bool Equals(object obj) => Equals(obj as Warehouse);
        public bool Equals(Warehouse warehouse) => warehouse != null && warehouse.Name == Name;
        public override string ToString() => Name;
    }
}