using System;

namespace WarehouseMonitor.Console.Domain
{
    public class Material
    {
        public string Id { get; private set; }
        public string Name { get; private set; }

        public Material(string id, string name)
        {
            if (string.IsNullOrWhiteSpace(id))
                throw new ArgumentNullException(nameof(id), "Material could not have an empty ID");

            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name), "Material could not have an empty name");

            Id = id;
            Name = name;
        }

        public override int GetHashCode() => Id.GetHashCode();
        public override bool Equals(object obj) => Equals(obj as Material);
        public bool Equals(Material material) => material != null && material.Id == Id;
        public override string ToString() => Id;
    }
}