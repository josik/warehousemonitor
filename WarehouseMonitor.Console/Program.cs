﻿using System.IO;
using WarehouseMonitor.Console.Infrastructure.In;
using WarehouseMonitor.Console.Infrastructure.Out;

namespace WarehouseMonitor.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputText = File.ReadAllText("Test.txt");

            IWarehouseParser parser = TextWarehouseParserFactory.Create();
            IWarehouseFormatter formatter = TextWarehousesFormatterFactory.Create();

            var warehouses = parser.Parse(inputText);
            var warehouseOutput = formatter.Format(warehouses);

            System.Console.WriteLine(warehouseOutput);
            System.Console.ReadKey();
        }
    }
}
