## Run project 

To run the project please follow such steps:

1. Clone the repository
2. Go to Warehouse.Console folder
3. Run command: dotnet run

---

## Small description

A few points to implementation:

1. I decided do not create separate projects for different layers and used folders as a layers, as a simplification for the test implementation.
2. Tests are written only for "In" flow for test test task reason ;)
3. It was hard to decided parent-child relation between material and warehouse, as in "real case scenario" it will be many-to-many relation, so I decided just for purpose of this narrow application, that relation Warehouse -> Materials will better fit.
4. Solution doesn't handle exceptions of incorrect input (just a domain models have some checks), as test task supposed to have only correct lines.
5. All solution is a bit overengineering, but I guess that was expected in terms of test task :)
