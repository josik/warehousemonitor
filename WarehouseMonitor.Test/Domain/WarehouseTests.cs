using System;
using System.Linq;
using WarehouseMonitor.Console.Domain;
using Xunit;

namespace WarehouseMonitor.Test.Domain
{
    public class WarehouseTests
    {
        [Fact]
        public void ShouldThrowExceptionIfNameIsEmpty()
        {
            var exception = Assert.Throws<ArgumentNullException>(() => new Warehouse(""));

            Assert.Equal("Warehouse could not have an empty name (Parameter 'name')", exception.Message);
        }

        [Fact]
        public void AddMaterial_ShouldAddNewMaterial()
        {
            var testWarehouse = new Warehouse("TestName");
            var testMaterial = new Material("id", "name");
            var materialAmount = 5;

            testWarehouse.AddMaterial(testMaterial, materialAmount);

            Assert.Equal(testWarehouse.Materials[testMaterial], materialAmount);
        }

        [Fact]
        public void AddMaterial_ShouldSumAmountToExistingMaterial()
        {
            var testWarehouse = new Warehouse("TestName");
            var testMaterial = new Material("id", "name");
            var startMaterialAmount = 5;
            var additinalMaterialAmount = 10;

            testWarehouse.AddMaterial(testMaterial, startMaterialAmount);
            testWarehouse.AddMaterial(testMaterial, additinalMaterialAmount);

            Assert.Equal(testWarehouse.Materials[testMaterial], startMaterialAmount + additinalMaterialAmount);
        }

        [Fact]
        public void AddMaterial_ShouldReturnOveralWahrehouseMaterialAmount()
        {
            var testWarehouse = new Warehouse("TestName");
            var testMaterial1 = new Material("id1", "name1");
            var testMaterial1Amount = 5;
            var testMaterial2 = new Material("id2", "name2");
            var testMaterial2Amount = 10;

            testWarehouse.AddMaterial(testMaterial1, testMaterial1Amount);
            testWarehouse.AddMaterial(testMaterial2, testMaterial2Amount);

            Assert.Equal(testWarehouse.MaterialsCount, testMaterial1Amount + testMaterial2Amount);
        }

        [Fact]
        public void ShouldCompareWarehouseByName()
        {
            var testWarehouse1 = new Warehouse("TestName");
            var testWarehouse2 = new Warehouse("TestName");

            Assert.Equal(testWarehouse1, testWarehouse2);
        }
    }
}