using System;
using WarehouseMonitor.Console.Domain;
using Xunit;

namespace WarehouseMonitor.Test.Domain
{
    public class MaterialTests
    {
        [Fact]
        public void ShouldThrowExceptionIfNameIsEmpty()
        {
            var exception = Assert.Throws<ArgumentNullException>(() => new Material("testId", ""));

            Assert.Equal("Material could not have an empty name (Parameter 'name')", exception.Message);
        }

        [Fact]
        public void ShouldThrowExceptionIfIdIsEmpty()
        {
            var exception = Assert.Throws<ArgumentNullException>(() => new Material("", "testName"));
            
            Assert.Equal("Material could not have an empty ID (Parameter 'id')", exception.Message);
        }

        [Fact]
        public void ShouldCompareMaterialById()
        {
            var testMaterial1 = new Material("testId1", "testName1");
            var testMaterial2 = new Material("testId1", "testName2");

            Assert.Equal(testMaterial1, testMaterial2);
        }
    }
}