using System.Linq;
using System.Text;
using WarehouseMonitor.Console.Infrastructure.In;
using Xunit;

namespace WarehouseMonitor.Test.Infrastructure.In
{
    public class TextWarehouseParseStrategyTests
    {
        private const string CORRECT_STRING = "Yankee Hardware 110 Deg. Hinge;COM-123908;WH-A,10|WH-B,11";
        private IWarehouseParseStrategy _strategy = new TextWarehouseParseStrategy();
        
        [Fact]
        public void Parse_ShouldReturnNullIfLineStartsWithSharp(){
            var commentLine = "#TEST LINE";
            var result = _strategy.Parse(commentLine);
            Assert.Null(result);
        }

        [Fact]
        public void Parse_ShouldCorrectlyParseMaterial(){
            var result = _strategy.Parse(CORRECT_STRING);

            Assert.NotNull(result.Material);
            Assert.Equal(result.Material.Id, "COM-123908");
            Assert.Equal(result.Material.Name, "Yankee Hardware 110 Deg. Hinge");
        }

        [Fact]
        public void Parse_ShouldCorrectlyParseAvailabilitiesCount(){
            var result = _strategy.Parse(CORRECT_STRING);

            Assert.NotNull(result.Availabilities);
            Assert.Equal(result.Availabilities.Count(), 2);
        }

        [Fact]
        public void Parse_ShouldCorrectlyParseAvailability(){
            var result = _strategy.Parse(CORRECT_STRING);
            var availability = result.Availabilities.First();
            
            Assert.NotNull(availability);
            Assert.Equal(availability.MaterialAmount, 10);
            Assert.Equal(availability.WarehouseName, "WH-A");
        }


        public void Split_ShouldCorrectlySplitInputText()
        {
            var multiLineText = new StringBuilder(CORRECT_STRING)
                .AppendLine(CORRECT_STRING)
                .ToString();

            var resultLines = _strategy.Split(multiLineText);

            Assert.Equal(resultLines.Count(), 2);
        }
    }
}