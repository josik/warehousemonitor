using System.Collections.Generic;
using System.Linq;
using Moq;
using WarehouseMonitor.Console.Domain;
using WarehouseMonitor.Console.Infrastructure.In;
using Xunit;
using static WarehouseMonitor.Console.Infrastructure.In.WarehouseImportItem;

namespace WarehouseMonitor.Test.Infrastructure.In
{
    public class WarehouseParserTests
    {
        private readonly WarehouseImportItem warehouseImportItem;

        public WarehouseParserTests()
        {
            warehouseImportItem = new WarehouseImportItem(new Material("abc", "testName1"),
                new List<WarehouseMaterialAvailability>()
                {
                    new WarehouseMaterialAvailability("TestWH1", 10),
                    new WarehouseMaterialAvailability("TestWH2", 5)
                }
            );
        }


        [Fact]
        public void Parse_ShouldIgnoreIncorrectLines()
        {
            var parser = SetUpParser(null);

            var result = parser.Parse(It.IsAny<string>());
            Assert.Empty(result);
        }

        [Fact]
        public void Parse_ShouldCorrectlyParseMaterial()
        {
            var parser = SetUpParser(warehouseImportItem);

            var resultWarehouse = parser.Parse(It.IsAny<string>()).First();
            Assert.Equal(warehouseImportItem.Material, resultWarehouse.Materials.First().Key);
        }

        [Fact]
        public void Parse_ShouldCorrectlyParseWarehouses()
        {
            var parser = SetUpParser(warehouseImportItem);

            var resultWarehouses = parser.Parse(It.IsAny<string>());
            Assert.True(resultWarehouses
                .All(warehouse => warehouseImportItem.Availabilities
                    .Select(availability => availability.WarehouseName)
                    .Contains(warehouse.Name)));
        }

        [Fact]
        public void Parse_ShouldCorrectlyParseAvailabilities()
        {
            var parser = SetUpParser(warehouseImportItem);

            var resultWarehouses = parser.Parse(It.IsAny<string>());

            Assert.True(warehouseImportItem.Availabilities.All(availability =>
                resultWarehouses.Any(warehouse =>
                {
                    return warehouse.Name == availability.WarehouseName &&
                        warehouse.Materials.Any(material =>
                            material.Key.Name == warehouseImportItem.Material.Name
                            && material.Value == availability.MaterialAmount);
                })));
        }

        [Fact]
        public void Parse_ShouldAvoidWarehouseDuplicates()
        {
            var parser = SetUpParser(warehouseImportItem, 2);

            var resultWarehouses = parser.Parse(It.IsAny<string>());

            Assert.Equal(resultWarehouses.Count(),
                warehouseImportItem.Availabilities.Select(a => a.WarehouseName).Distinct().Count());
        }

        private WarehouseParser SetUpParser(WarehouseImportItem returnParseItem, int lineCount = 1)
        {
            var strategyMock = new Mock<IWarehouseParseStrategy>();

            var lines = new List<string>();
            for (int i = 0; i < lineCount; i++) { lines.Add("Test"); }

            strategyMock.Setup(strategy => strategy.Split(It.IsAny<string>())).Returns(lines.ToArray());
            strategyMock.Setup(strategy => strategy.Parse(It.IsAny<string>())).Returns(() => returnParseItem);

            return new WarehouseParser(strategyMock.Object);
        }
    }
}